const mongoose = require("mongoose")

const taskSchema = new mongoose.Schema({
	name: {
		type: String,
		required: [true, "Name is required!"]
	},
	name: {
		type: String,
		default: "pending"
	}

});

module.exports = mongoose.model("Task", taskSchema)