const express = require("express");
const mongoose = require("mongoose");

const taskRoute = require('./Routes/taskRoutes.js')

const app = express();
const port = 3001;

	//connection
	mongoose.connect("mongodb+srv://admin:admin@batch245-sayosa.g75wxqn.mongodb.net/s35-discussion?retryWrites=true&w=majority", 
	{
		//allows us to avoid any and
		useNewUrlParser: true,
		useUnifiedTopology: true
	});

//check connection
	let db = mongoose.connection;

	//catcher
	db.on("error", console.error.bind(console, "Connetion error"));
	//confirmation of the connection
	db.once("open", ()=> console.log("We're connected to the cloud!"))


//Middlewares
app.use(express.json());
app.use(express.urlencoded({extended: true}));

//routing
app.use("/tasks", taskRoute);


app.listen(port, () => console.log(`Server is running at port ${port}`))



/*
	separation of concerns:
	1. model should be connnected to the controller.
	2. controller should be connected to the routes 
	3. route should be connected to the server/application



*/

