const Task = require("../Models/task.js")

// controllers and function


//controller to get all the task on our data base
module.exports.getAll = (request, response) => {

	Task.find({}).then(result => {
		// capture the result
		return response.send(result);
	})
	// catch captures the error when the find method is executed
	.catch(error =>{
		return response.send(error);
	})
}
module.exports.createTask = (request, response) => {
	const input = request.body;

	Task.findOne({name: input.name})
	.then(result =>{
		if(result !== null){
			return response.send("The task is already existing!")
		}else{
			let newTask = new Task({
				name: input.name
			});

			newTask.save().then(save => {
				return response.send("The task is successfully added!")
			}).catch(error=>{
				return response.send(error)
			})
		}
	})
	.catch(error =>{
		return response.send(error)
	})
}


//Controller that will delete

module.exports.deleteTask = (request, response) =>{
	let idToBeDeleted = request.params.id;
	// find by ID and remove find... find the document

	Task.findByIdAndRemove(idToBeDeleted)
	.then(result => {
		return response.send(result)
	})

	.catch(error => {
		return response.send(error);
	})

};


//(a1) Finding singleTask

module.exports.getOne = (request, response) => {

	let idToBeFound = request.params.id
	//using find by id for singleTask Get
	Task.findById(idToBeFound)
	.then(result =>{
		return response.send(result)
	})

	.catch(error =>{
		return response.send(error)
	})
}

module.exports.updateOne = (request, response) => {

	let idToBeUpdated = request.params.id;
	//using find by id for singleTask Get
	Task.findByIdAndUpdate(idToBeUpdated, { status: "completed" },{new:true})
	.then(result =>{
		return response.send(result)
	})

	.catch(error =>{
		return response.send(error)
	})
}