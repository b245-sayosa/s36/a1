const express = require("express");
const router = express.Router()


const taskController = require("../Controllers/taskControllers.js")



/*
routes
*/


//route for getAll
router.get("/", taskController.getAll);

//rout
router.post("/addTask", taskController.createTask)

//route for deletetask
router.delete("/deleteTasks/:id", taskController.deleteTask)

// (a1) creating a route for getting a specific task
router.get("/:id", taskController.getOne);

//(a1) creating a route for changing the status of the task
router.put("/:id/complete", taskController.updateOne)



module.exports = router